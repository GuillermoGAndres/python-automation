import os
import datetime

os.remove("filename.txt")
os.rename("old_name", "new_name")
os.path.exists("filename.txt")
os.path.getsize("filename.txt")
#para checar cuando fue la ultima modificacion del archivo se
#usa getmtime regresa un timestamp
#Unix timestamp represent number of second since 1 January 1970

timestamp = os.path.getmtime('filename.txt')
datetime.datetime.fromtimestamp(timestamp)

#Conocer la ruta absoluta de nuestro archivo
os.path.abspath('filename.txt')
#Current directory
os.getcwd()
os.mkdir("newDirectory")
#Cambiar directorio
os.chdir("newDirectory")
os.rmdir("newDirectory")
#Enlista todos los arhivos y directorios
os.listdir("newDirectory")
#Create the full path
os.path.join('dir', 'name')
